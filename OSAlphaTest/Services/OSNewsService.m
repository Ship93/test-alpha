//
//  OSNewsService.m
//  OSAlphaTest
//
//  Created by user on 27.05.17.
//  Copyright © 2017 Oleg Shipulin. All rights reserved.
//

#import "OSNewsService.h"
#import "OSRssParser.h"
#import "News.h"
#import "OSCoreDataStack.h"
#import "OSNewsDataModel+CoreDataClass.h"


static NSString *kUrlForNews = @"https://alfabank.ru/_/rss/_rss.html?subtype=3&category=2&city=21";

@interface OSNewsService () <OSRssParserDelegate>

@property (strong, nonatomic) OSCoreDataStack *coreDataStack;
@property (strong, nonatomic) OSRssParser *parser;

@end

@implementation OSNewsService

- (instancetype)init {
    if(self = [super init]) {
        _coreDataStack = [[OSCoreDataStack alloc] init];
        _parser = [[OSRssParser alloc] init];
        _parser.delegate = self;
    }
    return self;
}

- (void)downloadNews {
    self.parser.url = [NSURL URLWithString:kUrlForNews];
    [self.parser getDataFromRSS];
}

- (NSArray<News *> *)getNews {
    NSArray<OSNewsDataModel *> *newsModelArr = [self allNewsModel];
    NSMutableArray<News *> *newsArr = [NSMutableArray array];
    for (OSNewsDataModel* newsModel in newsModelArr) {
        News *news = [[News alloc] initWithModel:newsModel];
        [newsArr addObject:news];
    }
    return newsArr;
}

#pragma mark - OSRssParserDelegate
- (void)setNews:(NSArray<News *> *)news {
    [self.delegate showNews:news];
    [self removeAllNews];
    [self saveUpdatedNews:news];
}

#pragma mark - PrivateMethods
- (NSArray<OSNewsDataModel *> *)allNewsModel {
    NSFetchRequest<OSNewsDataModel *> *fetchRequest = [OSNewsDataModel fetchRequest];
    NSSortDescriptor *sortDateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"pubDate" ascending:YES];
    fetchRequest.sortDescriptors = @[sortDateDescriptor];
    NSError *error;
    NSArray<OSNewsDataModel *> *newsArr = [self.coreDataStack.context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"Fetch error: %@", error.localizedDescription);
    }
    return newsArr;
}

- (void)removeAllNews {
    
    NSArray<OSNewsDataModel *> *newsArr = [self allNewsModel];
    for (OSNewsDataModel *news in newsArr) {
        [self.coreDataStack.context deleteObject:news];
    }
}



- (void)saveUpdatedNews:(NSArray<News *> *)newsArr {
    for (News *news in newsArr) {
        OSNewsDataModel *newsModel = [NSEntityDescription insertNewObjectForEntityForName:@"OSNewsDataModel" inManagedObjectContext:self.coreDataStack.context];
        newsModel.title = news.title;
        newsModel.pubDate = news.pubDate;
        newsModel.link = news.link;
        newsModel.imageUrl = news.imageUrl;
    }
    
    [self.coreDataStack performSave:nil];
}

@end
