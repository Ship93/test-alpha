//
//  OSNewsService.h
//  OSAlphaTest
//
//  Created by user on 27.05.17.
//  Copyright © 2017 Oleg Shipulin. All rights reserved.
//

#import <Foundation/Foundation.h>

@class News;

@protocol OSNewsServiceDelegate <NSObject>

- (void)showNews:(NSArray<News *> *)news;

@end

@interface OSNewsService : NSObject

@property (weak, nonatomic) id<OSNewsServiceDelegate> delegate;

- (NSArray<News *> *)getNews;
- (void)downloadNews;

@end
