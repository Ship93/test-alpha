//
//  OSNewsDetailViewController.m
//  OSAlphaTest
//
//  Created by user on 28.05.17.
//  Copyright © 2017 Oleg Shipulin. All rights reserved.
//

#import "OSNewsDetailViewController.h"
#import "News.h"
#import "OSContentViewController.h"

#import <Social/Social.h>

@interface OSNewsDetailViewController () <UIPageViewControllerDataSource>

@property (strong, nonatomic) UIPageViewController *pageViewController;

@end

@implementation OSNewsDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    self.pageViewController.dataSource = self;
    
    OSContentViewController *startingViewController = [self viewControllerAtIndex:self.currentNewsIndex];
    NSArray<OSContentViewController *> *viewControllers = @[startingViewController];
    
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    [self addChildViewController:self.pageViewController];
    [self.view addSubview:self.pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    
    UIBarButtonItem *shareButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(showShareDialog:)];
    self.navigationItem.rightBarButtonItem = shareButton;
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (void)showShareDialog:(UIBarButtonItem *)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Поделиться" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *facebookAction = [UIAlertAction actionWithTitle:@"Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        SLComposeViewController *faceBookVC = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [faceBookVC setInitialText:self.newsArray[_currentNewsIndex].link];
        [self presentViewController:faceBookVC animated:YES completion:nil];
    }];
    
    UIAlertAction *twitterAction = [UIAlertAction actionWithTitle:@"Twitter" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        SLComposeViewController *faceBookVC = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [faceBookVC setInitialText:self.newsArray[_currentNewsIndex].link];
        [self presentViewController:faceBookVC animated:YES completion:nil];
    }];
    
    [alert addAction:facebookAction];
    [alert addAction:twitterAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (OSContentViewController *)viewControllerAtIndex:(NSUInteger)index {
    if (self.newsArray.count == 0 || index > self.newsArray.count) {
        return nil;
    }
    OSContentViewController *contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"OSContentViewController"];
    contentViewController.news = self.newsArray[index];
    contentViewController.index = index;
    
    return contentViewController;
}

- (void)viewWillLayoutSubviews {
    [self.view layoutIfNeeded];
}

#pragma mark - UIPageViewControllerDataSource
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    NSUInteger index = ((OSContentViewController *)viewController).index;
    NSLog(@"%lu", (unsigned long)index);
    if (index == 0 || index == NSNotFound) {
        self.currentNewsIndex = self.newsArray.count - 1;
    } else {
        self.currentNewsIndex = index - 1;
    }
    return [self viewControllerAtIndex:_currentNewsIndex];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    NSUInteger index = ((OSContentViewController *)viewController).index;
    NSLog(@"%lu", (unsigned long)index);
    if (index == self.newsArray.count - 1 || index == NSNotFound) {
        self.currentNewsIndex = 0;
    } else {
        self.currentNewsIndex = index + 1;
    }
    return [self viewControllerAtIndex:_currentNewsIndex];
}

@end
