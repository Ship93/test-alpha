//
//  OSContentViewController.h
//  OSAlphaTest
//
//  Created by user on 28.05.17.
//  Copyright © 2017 Oleg Shipulin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class News;

@interface OSContentViewController : UIViewController

@property (strong, nonatomic) News *news;
@property (assign, nonatomic) NSUInteger index;

@end
