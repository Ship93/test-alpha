//
//  ViewController.h
//  OSAlphaTest
//
//  Created by user on 27.05.17.
//  Copyright © 2017 Oleg Shipulin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class OSNewsService;

@interface ViewController : UIViewController

@property (strong, nonatomic) OSNewsService *service;

@end

