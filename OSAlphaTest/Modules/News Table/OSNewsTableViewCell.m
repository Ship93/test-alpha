//
//  OSNewsTableViewCell.m
//  OSAlphaTest
//
//  Created by user on 27.05.17.
//  Copyright © 2017 Oleg Shipulin. All rights reserved.
//

#import "OSNewsTableViewCell.h"
#import "News.h"

@interface OSNewsTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *pubDate;

@end

@implementation OSNewsTableViewCell

- (void)fillWithModel:(News *)model {
    self.title.text = model.title;
    self.pubDate.text = model.pubDate;
}

- (void)prepareForReuse {
    self.title.text = @"";
    self.pubDate.text = @"";
}

@end
