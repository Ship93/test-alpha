//
//  News.h
//  OSAlphaTest
//
//  Created by user on 27.05.17.
//  Copyright © 2017 Oleg Shipulin. All rights reserved.
//

#import <Foundation/Foundation.h>

@class OSNewsDataModel;

@interface News : NSObject

@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *pubDate;
@property (strong, nonatomic) NSString *link;
@property (strong, nonatomic) NSString *imageUrl;

- (instancetype)initWithModel:(OSNewsDataModel *)mode;

- (void)setCurrentPropertyValue:(NSString *)currentPropertyValue forCurrentProperty:(NSString *)currentProperty;

@end
