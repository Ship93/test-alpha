//
//  ViewController.m
//  OSAlphaTest
//
//  Created by user on 27.05.17.
//  Copyright © 2017 Oleg Shipulin. All rights reserved.
//

#import "ViewController.h"
#import "OSNewsTableViewCell.h"
#import "News.h"
#import "OSNewsService.h"
#import "OSNewsDetailViewController.h"
#import "OSAboutDeveloperViewController.h"

static NSString *kFirstAppLaunch = @"firstAppLaunch";


@interface ViewController () <UITableViewDelegate, UITableViewDataSource, OSNewsServiceDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray<News *> *newsArray;
@property (nonatomic, strong) UIRefreshControl* refreshControl;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.newsArray = [NSArray arrayWithObjects:[[News alloc] init], nil];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                             forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    
    //UIBarButtonItem *shareButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(aboutDeveloper:)];
    UIBarButtonItem *shareButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"developer"] style:UIBarButtonItemStylePlain target:self action:@selector(aboutDeveloper:)];
    self.navigationItem.rightBarButtonItem = shareButton;
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.tableView.estimatedRowHeight = 40;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(downloadNews) forControlEvents: UIControlEventValueChanged];
    [self.tableView insertSubview:self.refreshControl atIndex:0];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([userDefaults boolForKey:kFirstAppLaunch] == 0) {
        [self downloadNews];
        [userDefaults setObject:@1 forKey:kFirstAppLaunch];
    } else {
        [self getNews];
    }
}

- (void)setService:(OSNewsService *)service {
    _service = service;
    _service.delegate = self;
}

- (void)getNews {
    self.newsArray = [self.service getNews];
}

- (void)setNewsArray:(NSArray<News *> *)newsArray {
    [self.refreshControl endRefreshing];
    _newsArray = newsArray;
    [self.tableView reloadData];
}

- (void)downloadNews {
    [self.service downloadNews];
}

#pragma mark - Actions
- (void)aboutDeveloper:(UIBarButtonItem *)sender {
    OSAboutDeveloperViewController *vc = [[UIStoryboard storyboardWithName:@"AboutDeveloper" bundle:nil] instantiateInitialViewController];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.newsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *identifier = @"OSNewsTableViewCell";
    OSNewsTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:identifier];
    
    News *news = self.newsArray[indexPath.row];
    
    [cell fillWithModel:news];
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    OSNewsDetailViewController *vc = [[UIStoryboard storyboardWithName:@"NewsDetail" bundle:nil] instantiateInitialViewController];
    vc.newsArray = self.newsArray;
    vc.currentNewsIndex = indexPath.row;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    //[self.navigationController presentViewController:vc animated:YES completion:nil];
}

#pragma mark - OSNewsServiceDelegate
- (void)showNews:(NSArray<News *> *)news {
    self.newsArray = news;
}


@end
