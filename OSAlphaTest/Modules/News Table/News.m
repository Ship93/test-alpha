//
//  News.m
//  OSAlphaTest
//
//  Created by user on 27.05.17.
//  Copyright © 2017 Oleg Shipulin. All rights reserved.
//

#import "News.h"
#import "OSNewsDataModel+CoreDataClass.h"

@implementation News

- (instancetype)initWithModel:(OSNewsDataModel *)model {
    if(self = [super init]) {
        _title = model.title;
        _link = model.link;
        _pubDate = model.pubDate;
    }
    return self;
}

- (void)setCurrentPropertyValue:(NSString *)currentPropertyValue forCurrentProperty:(NSString *)currentProperty {
    if ([currentProperty isEqualToString:@"title"]) {
        self.title = currentPropertyValue;
    } else if ([currentProperty isEqualToString:@"link"]) {
        self.link = currentPropertyValue;
    } else if ([currentProperty isEqualToString:@"pubDate"]) {
        self.pubDate = currentPropertyValue;
    }
}

@end
