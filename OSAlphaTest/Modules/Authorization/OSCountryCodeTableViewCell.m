//
//  OSCountryCodeTableViewCell.m
//  OSAlphaTest
//
//  Created by user on 29.05.17.
//  Copyright © 2017 Oleg Shipulin. All rights reserved.
//

#import "OSCountryCodeTableViewCell.h"

@interface OSCountryCodeTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *nationalFlagImageView;
@property (weak, nonatomic) IBOutlet UILabel *nationalCodePhoneNumber;
@end

@implementation OSCountryCodeTableViewCell



@end
