//
//  OSAboutDeveloperViewController.m
//  OSAlphaTest
//
//  Created by user on 29.05.17.
//  Copyright © 2017 Oleg Shipulin. All rights reserved.
//

#import "OSAboutDeveloperViewController.h"
#import "UIColor+OSColor.h"

typedef enum : NSUInteger {
    OSNextAnimationName,
    OSNextAnimationPS,
    OSNextAnimationPhoneNumber,
    OSNextAnimationNone,
} OSNextAnimation;

@interface OSAboutDeveloperViewController ()

@property (weak, nonatomic) IBOutlet UIView *tapView;
@property (assign, nonatomic) OSNextAnimation nextAnimation;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nameLabelTopLayoutConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *postScriptumLAbelLeadingLayoutConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *phoneNumberButtonTopLayoutConstraint;

@end

@implementation OSAboutDeveloperViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
    [self.view addGestureRecognizer:tap];
    
    self.tapView.layer.masksToBounds = NO;
    self.tapView.layer.cornerRadius = self.tapView.bounds.size.height / 2;
    self.tapView.hidden = YES;
    
    self.nextAnimation = OSNextAnimationName;
    
    self.nameLabelTopLayoutConstraint.constant = -300.f;
    self.postScriptumLAbelLeadingLayoutConstraint.constant = -1000.f;
    self.phoneNumberButtonTopLayoutConstraint.constant = 600;
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.title = @"Обо мне";
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self descriptionWithAnimate];
}

- (void)tap:(UITapGestureRecognizer *)sender {
    self.tapView.hidden = NO;
    self.tapView.alpha = 1.f;
    CGPoint point = [sender locationInView:self.view];
    self.tapView.center = point;
    CGRect frame =  self.tapView.frame;
    frame = CGRectMake(point.x - 5, point.y - 5, 10, 10);
    self.tapView.frame = frame;
    self.tapView.layer.cornerRadius = self.tapView.bounds.size.height / 2;
    frame = CGRectMake(point.x - 50, point.y - 50, 100, 100);
    
    [UIView animateWithDuration:1.3f animations:^{
        self.tapView.frame = frame;
        self.tapView.layer.cornerRadius = self.tapView.bounds.size.height / 2;
        self.tapView.backgroundColor = [UIColor randomColor];
        self.tapView.alpha = 0.5f;
    } completion:^(BOOL finished) {
        self.tapView.hidden = YES;
    }];
    
    [self descriptionWithAnimate];
}

- (void)descriptionWithAnimate {
    CGFloat duration = 0.5f;
    CGFloat damping = 0.2f;
    switch (_nextAnimation) {
        case OSNextAnimationName: {
            [UIView animateWithDuration:duration delay:0
                 usingSpringWithDamping:damping - 0.1f initialSpringVelocity:0.0f
                                options:0 animations:^{
                                    self.nameLabelTopLayoutConstraint.constant = 10.f;
                                    [self.view layoutIfNeeded];
                                } completion:nil];
            break;
        }
        case OSNextAnimationPS: {
            [UIView animateWithDuration:duration delay:0
                 usingSpringWithDamping:damping initialSpringVelocity:0.0f
                                options:0 animations:^{
                                    self.postScriptumLAbelLeadingLayoutConstraint.constant = 30.f;
                                    [self.view layoutIfNeeded];
                                } completion:nil];
            break;
        }
        case OSNextAnimationPhoneNumber: {
            [UIView animateWithDuration:duration delay:0
                 usingSpringWithDamping:damping - 0.2f initialSpringVelocity:0.0f
                                options:0 animations:^{
                                    self.phoneNumberButtonTopLayoutConstraint.constant = 130;
                                    [self.view layoutIfNeeded];
                                } completion:nil];
            self.nextAnimation++;
            break;
        }
        case OSNextAnimationNone:
            break;
    }
    self.nextAnimation++;
}

- (IBAction)callAction:(id)sender {
    
    NSString *phNo = @"+79253458042";
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    } else {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Ошибка" message:@"Не могу набрать" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Не могу набрать" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) { }];
        
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

@end
