//
//  OSCoreDataStack.m
//  OSAlphaTest
//
//  Created by user on 27.05.17.
//  Copyright © 2017 Oleg Shipulin. All rights reserved.
//

#import "OSCoreDataStack.h"

static NSString *managedObjectModelName = @"CoreDataModel";

@interface OSCoreDataStack ()

@property (strong, nonatomic) NSURL *storeUrl;

@end

@implementation OSCoreDataStack

- (NSURL *)storeUrl {
    if (_storeUrl == nil) {
        NSURL *documentDirUrl = [[NSFileManager.defaultManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] firstObject];
        _storeUrl = [documentDirUrl URLByAppendingPathComponent:@"Store.sqlite"];
    }
    
    return  _storeUrl;
}

- (NSManagedObjectModel *)managedObjectModel {
    
    if (_managedObjectModel == nil) {
        NSURL *modelUrl = [NSBundle.mainBundle URLForResource:managedObjectModelName withExtension:@"momd"];
        if (modelUrl) {
            _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelUrl];
        } else {
            NSLog(@"Empty model url!");
        }
    }
    
    return  _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)coordinator {
    
    if (_coordinator == nil) {
        if (self.managedObjectModel) {
            _coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:_managedObjectModel];
            NSError *error;
            [_coordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:self.storeUrl options:nil error: &error];
            if (error) {
                NSLog(@"Error adding persistent store to coordinator: %@", error.localizedDescription);
            }
        } else {
            NSLog(@"Empty managed object model");
        }
    }
    return  _coordinator;
}


- (NSManagedObjectContext *)context {
    
    if (_context == nil) {
        _context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        _context.persistentStoreCoordinator = self.coordinator;
        _context.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy;
    }
    return  _context;
}

- (void)performSave:(void(^)(void))completionHandler {
    if (self.context.hasChanges) {
        [_context performBlock:^{
            NSError *error;
            [_context save:&error];
            if (error) {
                NSLog(@"Context save error: %@", error.localizedDescription);
            }
            
            if (completionHandler) {
                completionHandler();
            }
        }];
    } else if (completionHandler) {
        completionHandler();
    }
}




@end
