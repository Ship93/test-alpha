//
//  OSRssParser.m
//  OSAlphaTest
//
//  Created by user on 27.05.17.
//  Copyright © 2017 Oleg Shipulin. All rights reserved.
//

#import "OSRssParser.h"
#import "News.h"

@interface OSRssParser () <NSXMLParserDelegate>

@property (strong, nonatomic) NSXMLParser *parser;
@property (strong, nonatomic) News *currentItem;
@property (strong, nonatomic) NSString *currentProperty;
@property (strong, nonatomic) NSMutableString *currentPropertyValue;

@property (strong, nonatomic) NSMutableArray<News *> *newsItems;


@end

@implementation OSRssParser

-(void) getDataFromRSS {
    _parser = [[NSXMLParser alloc] initWithContentsOfURL:_url];
    _parser.delegate = self;
    [_parser parse];
}

- (NSMutableArray<News *> *)newsItems {
    if (_newsItems == nil) {
        _newsItems = [[NSMutableArray alloc] init];
    }
    return _newsItems;
}

#pragma mark - NSXMLParserDelegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    if ( [elementName isEqualToString:@"item"] ) {
        self.currentItem = [[News alloc] init];
        return;
        
    }
    
    else if ([elementName isEqualToString:@"title"] || [elementName isEqualToString:@"link"] || [elementName isEqualToString:@"description"] || [elementName isEqualToString:@"pubDate"]) {
        self.currentProperty = elementName;
        return;
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    if (!self.currentPropertyValue) {
        self.currentPropertyValue = [[NSMutableString alloc] initWithCapacity:50];
    }
    [self.currentPropertyValue appendString:string];
    
    NSString *trimmedString = [self.currentPropertyValue stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"\t\n"]];
    [self.currentPropertyValue setString:trimmedString];
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    
    if ([elementName isEqualToString:@"item"]) {
        [self.newsItems addObject:_currentItem];
        //self.currentItem = nil;
        return;
    }
    
    else if ([elementName isEqualToString:_currentProperty]){
        [self.currentItem setCurrentPropertyValue:self.currentPropertyValue forCurrentProperty:self.currentProperty];
    }
    
    _currentPropertyValue = nil;
}

- (void)parserDidStartDocument:(NSXMLParser *)parser {
    NSLog(@"parserDidStartDocument");
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    NSLog(@"parserDidEndDocument");
    [self.delegate setNews:self.newsItems];
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    NSLog(@"parseErrorOccurred: %@",  parseError.localizedDescription);
}

- (void)parser:(NSXMLParser *)parser validationErrorOccurred:(NSError *)validationError {
    NSLog(@"validationErrorOccurred: %@",  validationError.localizedDescription);
}

@end
