//
//  OSRssParser.h
//  OSAlphaTest
//
//  Created by user on 27.05.17.
//  Copyright © 2017 Oleg Shipulin. All rights reserved.
//

#import <Foundation/Foundation.h>

@class News;

@protocol OSRssParserDelegate <NSObject>

- (void)setNews:(NSArray<News *> *)news;

@end

@interface OSRssParser : NSObject

@property (strong, nonatomic) NSURL *url;
@property (weak, nonatomic) id<OSRssParserDelegate> delegate;

-(void) getDataFromRSS;

@end
