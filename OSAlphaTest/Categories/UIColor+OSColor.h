//
//  UIColor+OSColor.h
//  OSAlphaTest
//
//  Created by user on 30.05.17.
//  Copyright © 2017 Oleg Shipulin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (OSColor)

+ (UIColor *)randomColor;

@end
