//
//  UIColor+OSColor.m
//  OSAlphaTest
//
//  Created by user on 30.05.17.
//  Copyright © 2017 Oleg Shipulin. All rights reserved.
//

#import "UIColor+OSColor.h"

@implementation UIColor (OSColor)

+ (UIColor *)randomColor {
    CGFloat hue = ( arc4random() % 256 / 256.0 );
    CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;
    CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;
    return [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
}

@end
